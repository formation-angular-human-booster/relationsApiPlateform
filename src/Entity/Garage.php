<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\GarageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 * @ORM\Entity(repositoryClass=GarageRepository::class)
 */
class Garage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read", "write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read", "write"})
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=Voiture::class, mappedBy="garage")
     */
    private  $voitures;

    /**
     * @return mixed
     */
    public function getVoitures()
    {
        return $this->voitures;
    }

    /**
     * @param mixed $voitures
     */
    public function setVoitures($voitures): void
    {
        $this->voitures = $voitures;
    }

    public function __construct()
    {
        $this->voitures = new ArrayCollection();
    }



    public function addVoiture(Voiture $voiture){
        if(!$this->voitures->contains($voiture)){
            $this->voitures->add($voiture);
        }
    }

    public function removeVoiture(Voiture $voiture){
        if($this->voitures->contains($voiture)){
            $this->voitures->removeElement($voiture);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
}
