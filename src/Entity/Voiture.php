<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\VoitureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 * @ORM\Entity(repositoryClass=VoitureRepository::class)
 *
 */
class Voiture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read", "write"})
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity=Garage::class, inversedBy="voitures")
     * @Groups("full")
     * @Groups({"read", "write"})
     */
    private $garage;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"full"})
     * @Groups({"read", "write"})
     */
    private $nom;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGarage(): Garage
    {
        return $this->garage;
    }

    public function setGarage(Garage $garage): Garage
    {
        $this->garage = $garage;

        return $this->garage;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }


}
