<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200605121325 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE voiture ADD garrage_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE voiture ADD CONSTRAINT FK_E9E2810F7369D9C6 FOREIGN KEY (garrage_id) REFERENCES garage (id)');
        $this->addSql('CREATE INDEX IDX_E9E2810F7369D9C6 ON voiture (garrage_id)');
        $this->addSql('ALTER TABLE garage DROP FOREIGN KEY FK_9F26610B181A8BA');
        $this->addSql('DROP INDEX IDX_9F26610B181A8BA ON garage');
        $this->addSql('ALTER TABLE garage DROP voiture_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE garage ADD voiture_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE garage ADD CONSTRAINT FK_9F26610B181A8BA FOREIGN KEY (voiture_id) REFERENCES voiture (id)');
        $this->addSql('CREATE INDEX IDX_9F26610B181A8BA ON garage (voiture_id)');
        $this->addSql('ALTER TABLE voiture DROP FOREIGN KEY FK_E9E2810F7369D9C6');
        $this->addSql('DROP INDEX IDX_E9E2810F7369D9C6 ON voiture');
        $this->addSql('ALTER TABLE voiture DROP garrage_id');
    }
}
